# Informační systém pracovního rozvrhu
## 1. Vize
V tomto informačním systému bude možno vytvářet rozvrh pracovní doby. Systém je vytvářen z důvodu přehledu zaměstnavatele o tom, kdy jeho zaměstnanci vykonávají práci a kdy jsou dostupní. Systém bude umožňovat určit od kdy do kdy je daný zaměstnanec v práci, jestli je v daný den na home office nebo jestli nepracuje. To je určeno na základě smlouvy, ve které je uveden hodinový rozpočet na jeden týden.

Systém bude podporovat různé platformy. Webové rozhraní pro všechny uživatele, kde uživatelé budou spravovat svůj rozvrh a popřípadě své subjekty. A mobilní aplikaci pro manažery pro správu jejich subjektů.

V tomto systému by mělo být několik druhů uživatelů. Zaměstnanec, který má možnost zadávat svůj pracovní rozvrh podle své smlouvy a zadávat žádost o dovolenou. 

Dalším typem uživatele je Manažer, který má stejné možnosti jako Zaměstnanec. Dále může prohlížet rozvrhy zaměstnanců jež pod něj spadají a schvaluje jejich dovolené. Manažer může přidávat Zaměstnance nebo je předat jinému Manažeru. Dále také přidává pracovní smlouvy a určuje jejich rozsah. 

Poslední typem uživatele je správce systému, který se stará o tom, aby systém fungoval plynule a bez potíží. Dále řeší případné problémy a aktualizace.	

Systém bude provozován na soukromých serverech subjektu a bude dostupný v době dostupnosti serveru. Uživatelům budou oznámeny plánované výpadky systému v závislosti na dostupnosti serveru. Systém bude fungovat na relační databázi.
***

## 2. Use case
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_diagram.png "use case diagram")
### 2.1 UC01 - nasazení nové verze systému
**popis:** Po úspěšném nahlášení odstávky systému, provede administrátor nasazené nové verze systému. Poté systém uvědomí své uživatele o znovu dostupnosti.

**účastníci:** systém, správce systému

**vstupní podmínka:** nová verze systému je funkční

**hlavní tok:**
1.	správce nahlásí odstavení systému s dostatečným předstihem
2.	správce odstaví systém
3.	systém se stane nedostupným pro uživatele
4.	správce nahraje novou verzi systému
5.	správce otestuje funkčnost nasazené verze
6.	správce spustí systém
7.	systém informuje správce o spuštění
8.	systém informuje uživatele o své dostupnosti

**alternativní tok:**
*   3. a	systém zůstane dostupný uživatelům
*   3. b	návrat ke kroku 2.
*   5. a	nová nasazená verze není správně funkční
*   5. b	správce upraví stávající verzi do funkční podoby
*   5. c	správce pokračuje od kroku 4.

**výstupní podmínka:** systém je uveden do provozuschopného stavu    

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_01_state.png "use case 01 state")

### 2.2 UC02 - schválení dovolené
**popis:** Manažer provede schválení dovolené v případě, že zaměstnanec má dostatečný počet dní nevyčerpané dovolené. V opačném případě systém uvědomí manažera o nedostatečném počtu dní a dovolené bude zamítnuta.

**účastníci**: systém, manažer

**vstupní podmínka**: manažer má zaměstnance, kteří zažádali o dovolenou

**hlavní tok**:
1. 	manažer si zobrazí žádosti o dovolenou
2.	manažer vybere zaměstnance
3.	manažer pošle požadavek na schválení dovolené
4.	systém zpracuje požadavek na základě zbylých volných dnů daného zaměstnance
5.	systém změní stav dovolené na schválenou
6. 	systém upozorní manažera a zaměstnance o schválení dovolené

**alternativní tok**:
*   4.a	    zaměstnanec nemá dostatečný zbývající počet dní dovolené
*   4.b	    systém změní stav dovolené na zamítnutou
*   4.c	    systém upozorní manažera a zaměstnance o nedostatku volných dní
*   4.d	    návrat ke kroku 1.	

**výstupní podmínka**: vybraná dovolené je uvedena do stavu schválené nebo neschválená	

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_02_state.png "use case 02 state")

### 2.3 UC03 – vytvoření rozvrhu
**popis:** Zaměstnanec si volí pracovní rozvrh pro jednotlivé dny. Vytváření rozvrhu probíhá po krocích pro každý jednotlivý den v týdnu nebo do vyčerpání rozpočtu hodin pro daný týden. V případě nevypotřebování všech hodin je zaměstnanec vyzván k dopolnění.

**účastníci**: systém, zaměstnanec

**vstupná podmínka**: zaměstnanec je přihlášen do systému

**hlavní tok**: 
1.	zaměstnanec zvolí den ve svém rozvrhu
2. 	zaměstnanec zvolí kolik hodin bude v daný den pracovat
3. 	zaměstnanec zvolí v kolik hodin začne v daný den pracovat
4.	zaměstnanec zvolí místo výkonu práce
5. 	zaměstnanec potvrdí změny pro daný den
6. 	systém zkontroluje změny
7. 	systém potvrdí změny
8. 	systém uloží rozvrh

**alternativní tok**:
*   7.a	    zaměstnanec zvolil více hodin, než má uvedených ve smlouvě
*   7.b	    systém zamítne změny
*   7.c	    systém vyzve uživatele ke změně rozvrhu
*   7.d     návrat ke kroku 1.
*   8.a	    zaměstnanec nezvolil plný počet hodin, z rozsahu své smlouvy
*   8.b	    systém neuloží rozvrh
*   8.c	    systém vyzve zaměstnance k další volbě
*   8.d	    návrat ke kroku 1.

**výstupní podmínka**: zaměstnanec zvolil všechny hodiny na své smlouvě

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_03_state.png "use case 02 state")

## 3. Technická specifikace
### 3.1 Konceptuální model domény

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/technicka_specifikace/konceptualni.png "konceptualni model domeny")

### 3.2 Technologie
Systém bude založen na technoligii JavaEEa Spring Boot v programovacím jazyce Java. Systém bude rozdělen na Databázový a Aplikační server. Jako databázový server bude použit Oracle SQL server. Databázový server nebude připojen k internetu. Aplikační setver bude poskytovat rozhraní pro komunikaci mezi uživatelskou aplikací a databázovým serverem.
#### 3.2.1 Aplikační server
**popis:** Tento server bude obsahovat jeho aplikační logiku systému skrytou za REST rozhraním, ke které budou moci klienti přistupovat přes internet z mobilního nebo webového rozhraní. Server bude připojen standartní přípojkou RJ-45 s rychlostí 100Mbps. V případě, že průměrný počet uživatelů odpovádá 120, minimální nárok na síť je v tu chíli 33KiB. Z toho vyplývá, že by stačilo připojené o rychlosti 10Mbps. 100Mbps připojení bylo zvoleno z důvodů, budoucího růstu počtu uživatelů a budoucí vyšší nároky na přenesaná data.

**operační systém:** Linux

**software:** JavaEE, Tomcat

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 500GB HDD
#### 3.2.2 Databázový server
**popis:** Tento server bude obsahovat databázi informačního systému, ke které bude aplikační server přistupovat přes místní sít. Tento server bude mít dva pevné disky zapojené v módu RAID 1 (zrcadlení) pro možnost obnovení dat při potenciální poruše disku.

**operační systém:** Linux

**software:** JavaEE, Oracle SQL server

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 2x 1000GB HDD
#### 3.2.3 Webový klient
**popis:** Uživatelé budou moci přistupovat do systému po přihlášení. Systém bude vytvořen pomocí ReactJS a používat Matrial design guidelines. Server bude připojen stejně jako aplikační server.

**operační systém:** Linux

**software:** Aktuální verze klientu, NodeJS

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 500GB HDD
#### 3.2.4 Mobilní klient
**popis:** Uživatelé budou moci přistupovat k systému pomocí aplikace pro systém Android

**operační systém:** Android OS
### 3.3 Odhad objemu dat

Entita | Velikost [B] | Počet | Celková velikost [KiB]
------ | ------------ | ----- | ----------------------
User | 30 | 500 | 16
Vacation | 30 | 5500 | 162
ScheduleDay | 64 | 3500 | 220
Contract | 120 | 700 | 84
DayType | 10 | 7 | 1
UserRole | 10 | 3 | 1
ContractType | 16 | 10 | 1

**Celková velikost dat:** 490KiB

**Odhadovaný počet připojených uživatelů v daný moment:** 120

## 4. Návrh uživatelského rozhraní
### 4.1 My schedule
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/1.1-scheduler.png "my schedule")
### 4.2 My people
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/2.1-admin.png "my people")
### 4.3 New contract
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/3.1-contract.png "new contract")

## 5. Návrh doménového modelu
### 5.1 Třídní diagram
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/class_diagram.png "class diagram")
### 5.2 Sekvenční diagramy
#### 5.2.1 Sekvenční diagram pro UC01
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/sequence01.png "sequence01")
#### 5.2.2 Sekvenční diagram pro UC02
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/sequence02.png "sequence02")
### 5.3 Použité návrhové vzory

*   Enity
*   Builder
*   Row data gateway
*   Bridge
*   Domain Model
*   Data mapper
*   Unit of work
*   Lazy load

## 6. Popis architektury systému
### 6.1 Diagram nasazení
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/architektura_systemu/diagram_nasazeni.png "deploy")
### 6.2 Diagram komponent
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/architektura_systemu/diagram_komponent.png "component")