# Vize
V tomto informačním systému bude možno vytvářet rozvrh pracovní doby. Systém je vytvářen z důvodu přehledu zaměstnavatele o tom, kdy jeho zaměstnanci vykonávají práci a kdy jsou dostupní. Systém bude umožňovat určit od kdy do kdy je daný zaměstnanec v práci, jestli je v daný den na home office nebo jestli nepracuje. To je určeno na základě smlouvy, ve které je uveden hodinový rozpočet na jeden týden.

Systém bude podporovat různé platformy. Webové rozhraní pro všechny uživatele, kde uživatelé budou spravovat svůj rozvrh a popřípadě své subjekty. A mobilní aplikaci pro manažery pro správu jejich subjektů.

V tomto systému by mělo být několik druhů uživatelů. Zaměstnanec, který má možnost zadávat svůj pracovní rozvrh podle své smlouvy a zadávat žádost o dovolenou. 

Dalším typem uživatele je Manažer, který má stejné možnosti jako Zaměstnanec. Dále může prohlížet rozvrhy zaměstnanců jež pod něj spadají a schvaluje jejich dovolené. Manažer může přidávat Zaměstnance nebo je předat jinému Manažeru. Dále také přidává pracovní smlouvy a určuje jejich rozsah. 

Poslední typem uživatele je správce systému, který se stará o tom, aby systém fungoval plynule a bez potíží. Dále řeší případné problémy a aktualizace.	

Systém bude provozován na soukromých serverech subjektu a bude dostupný v době dostupnosti serveru. Uživatelům budou oznámeny plánované výpadky systému v závislosti na dostupnosti serveru. Systém bude fungovat na relační databázi. 