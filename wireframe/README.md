# Návrh uživatelského rozhraní
## My schedule
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/1.1-scheduler.png "my schedule")
## My people
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/2.1-admin.png "my people")
## New contract
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/wireframe/3.1-contract.png "new contract")