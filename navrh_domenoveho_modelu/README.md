# Návrh doménového modelu
## Třídní diagram
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/class_diagram.png "class diagram")
## Sekvenční diagramy
### Sekvenční diagram pro UC01
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/sequence01.png "sequence01")
### Sekvenční diagram pro UC02
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/navrh_domenoveho_modelu/sequence02.png "sequence02")
## Použité návrhové vzory

*   Enity
*   Builder
*   Row data gateway
*   Bridge
*   Domain Model
*   Data mapper
*   Unit of work
*   Lazy load