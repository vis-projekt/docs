# Use case
![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_diagram.png "use case diagram")
### UC01 - nasazení nové verze systému
**popis:** Po úspěšném nahlášení odstávky systému, provede administrátor nasazené nové verze systému. Poté systém uvědomí své uživatele o znovu dostupnosti.

**účastníci:** systém, správce systému

**vstupní podmínka:** nová verze systému je funkční

**hlavní tok:**
1.	správce nahlásí odstavení systému s dostatečným předstihem
2.	správce odstaví systém
3.	systém se stane nedostupným pro uživatele
4.	správce nahraje novou verzi systému
5.	správce otestuje funkčnost nasazené verze
6.	správce spustí systém
7.	systém informuje správce o spuštění
8.	systém informuje uživatele o své dostupnosti

**alternativní tok:**
*   3. a	systém zůstane dostupný uživatelům
*   3. b	návrat ke kroku 2.
*   5. a	nová nasazená verze není správně funkční
*   5. b	správce upraví stávající verzi do funkční podoby
*   5. c	správce pokračuje od kroku 4.

**výstupní podmínka:** systém je uveden do provozuschopného stavu    

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_01_state.png "use case 01 state")

### UC02 - schválení dovolené
**popis:** Manažer provede schválení dovolené v případě, že zaměstnanec má dostatečný počet dní nevyčerpané dovolené. V opačném případě systém uvědomí manažera o nedostatečném počtu dní a dovolené bude zamítnuta.

**účastníci**: systém, manažer

**vstupní podmínka**: manažer má zaměstnance, kteří zažádali o dovolenou

**hlavní tok**:
1. 	manažer si zobrazí žádosti o dovolenou
2.	manažer vybere zaměstnance
3.	manažer pošle požadavek na schválení dovolené
4.	systém zpracuje požadavek na základě zbylých volných dnů daného zaměstnance
5.	systém změní stav dovolené na schválenou
6. 	systém upozorní manažera a zaměstnance o schválení dovolené

**alternativní tok**:
*   4.a	    zaměstnanec nemá dostatečný zbývající počet dní dovolené
*   4.b	    systém změní stav dovolené na zamítnutou
*   4.c	    systém upozorní manažera a zaměstnance o nedostatku volných dní
*   4.d	    návrat ke kroku 1.	

**výstupní podmínka**: vybraná dovolené je uvedena do stavu schválené nebo neschválená	

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_02_state.png "use case 02 state")

### UC03 – vytvoření rozvrhu
**popis:** Zaměstnanec si volí pracovní rozvrh pro jednotlivé dny. Vytváření rozvrhu probíhá po krocích pro každý jednotlivý den v týdnu nebo do vyčerpání rozpočtu hodin pro daný týden. V případě nevypotřebování všech hodin je zaměstnanec vyzván k dopolnění.

**účastníci**: systém, zaměstnanec

**vstupná podmínka**: zaměstnanec je přihlášen do systému

**hlavní tok**: 
1.	zaměstnanec zvolí den ve svém rozvrhu
2. 	zaměstnanec zvolí kolik hodin bude v daný den pracovat
3. 	zaměstnanec zvolí v kolik hodin začne v daný den pracovat
4.	zaměstnanec zvolí místo výkonu práce
5. 	zaměstnanec potvrdí změny pro daný den
6. 	systém zkontroluje změny
7. 	systém potvrdí změny
8. 	systém uloží rozvrh

**alternativní tok**:
*   7.a	    zaměstnanec zvolil více hodin, než má uvedených ve smlouvě
*   7.b	    systém zamítne změny
*   7.c	    systém vyzve uživatele ke změně rozvrhu
*   7.d     návrat ke kroku 1.
*   8.a	    zaměstnanec nezvolil plný počet hodin, z rozsahu své smlouvy
*   8.b	    systém neuloží rozvrh
*   8.c	    systém vyzve zaměstnance k další volbě
*   8.d	    návrat ke kroku 1.

**výstupní podmínka**: zaměstnanec zvolil všechny hodiny na své smlouvě

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/use_case/use_case_03_state.png "use case 02 state")