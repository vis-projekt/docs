# Technická specifikace
## Konceptuální model domény

![alt text](https://gitlab.com/vis-projekt/docs/raw/master/technicka_specifikace/konceptualni.png "konceptualni model domeny")

## Technologie
Systém bude založen na technoligii JavaEE a Spring Boot v programovacím jazyce Java. Systém bude rozdělen na Databázový a Aplikační server. Jako databázový server bude použit Oracle SQL server. Databázový server nebude připojen k internetu. Aplikační setver bude poskytovat rozhraní pro komunikaci mezi uživatelskou aplikací a databázovým serverem.
### Aplikační server
**popis:** Tento server bude obsahovat jeho aplikační logiku systému skrytou za REST rozhraním, ke které budou moci klienti přistupovat přes internet z mobilního nebo webového rozhraní. Server bude připojen standartní přípojkou RJ-45 s rychlostí 100Mbps. V případě, že průměrný počet uživatelů odpovádá 120, minimální nárok na síť je v tu chíli 33KiB. Z toho vyplývá, že by stačilo připojené o rychlosti 10Mbps. 100Mbps připojení bylo zvoleno z důvodů, budoucího růstu počtu uživatelů a budoucí vyšší nároky na přenesaná data.

**operační systém:** Linux

**software:** JavaEE, Tomcat

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 500GB HDD
### Databázový server
**popis:** Tento server bude obsahovat databázi informačního systému, ke které bude aplikační server přistupovat přes místní sít. Tento server bude mít dva pevné disky zapojené v módu RAID 1 (zrcadlení) pro možnost obnovení dat při potenciální poruše disku.

**operační systém:** Linux

**software:** JavaEE, Oracle SQL server

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 2x 1000GB HDD
### Webový klient
**popis:** Uživatelé budou moci přistupovat do systému po přihlášení. Systém bude vytvořen pomocí ReactJS a používat Matrial design guidelines. Server bude připojen stejně jako aplikační server.

**operační systém:** Linux

**software:** Aktuální verze klientu, NodeJS

**hardware:** Intel Xeon E5-2450 (2.1GHz), 32GB RAM, 500GB HDD
### Mobilní klient
**popis:** Uživatelé budou moci přistupovat k systému pomocí aplikace pro systém Android

**operační systém:** Android OS
### Odhad objemu dat

Entita | Velikost [B] | Počet | Celková velikost [KiB]
------ | ------------ | ----- | ----------------------
User | 30 | 500 | 16
Vacation | 30 | 5500 | 162
ScheduleDay | 64 | 3500 | 220
Contract | 120 | 700 | 84
DayType | 10 | 7 | 1
UserRole | 10 | 3 | 1
ContractType | 16 | 10 | 1

**Celková velikost dat:** 490KiB

**Odhadovaný počet připojených uživatelů v daný moment:** 120